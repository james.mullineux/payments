Maven build:
```mvn clean package```

Maven test:
```mvn test```

Maven run:
```mvn spring-boot:run```

Or share and run the jar directly:
```java -jar target/Payments-0.1.0.jar```

NB: To run the tests locally you will need a mongodb instance running on port 27017. The unit tests create a database called 'test' and the acceptance tests create a database called 'acceptanceTests', each creates a single colleciton called 'Payments'.

I have included the swagger.json that can be imported into swagger.io to see the REST API design. 

Several trade offs were made:
1. I used spring boot to simplify the REST API layer but not the mongo plugin. This uses HAL(hypermedia application language), and even when not using HAL it uses a HAL like syntax, which would change the format of the response and I wanted to prioritise extensibility over extra features. 
4. The acceptance tests read the java object directly, automatically deserialising the pojo. This makes the java client much cleaner but has its limitations. For example if you were to change one of the acceptance tests to read a String instead of a Payment from the ResponseEntity it would mainly work as json, but the ObjectId from bson needs to be deserialised properly otherwise it just tries to print out its internal state and doesn't show you the id being used by mongo. 

Note: As per the mongodb domain model, and the semantics of PUT, a full update will be performed when updating the document. 