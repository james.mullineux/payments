package com.jamesmullineux.payments;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.ArrayList;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpEntity;

import com.jamesmullineux.payments.model.Payment;
import com.jamesmullineux.payments.model.Attributes;
import com.jamesmullineux.payments.model.BeneficiaryParty;
import com.jamesmullineux.payments.model.ChargesInformation;
import com.jamesmullineux.payments.model.SenderCharges;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class HttpAcceptanceTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    String root;

    @Before
    public void beforeEach() {
        root = "http://localhost:" + port + "/api/v1/";
    }

    @Test
    public void getToStatusUrlShouldReturnDefaultMessage() throws Exception {
        assertThat(this.restTemplate.getForObject(root + "status",
            String.class)).contains("Payments Service Up");
    }

    @Test
    public void postPaymentShouldReturnStatusCodeOkAndAnId() throws Exception {
        ObjectId orgid = new ObjectId();
        Payment payment = new Payment(orgid);

        ResponseEntity<ObjectId> response = this.restTemplate.exchange(
            root + "payments", 
            HttpMethod.POST, 
            new HttpEntity<Payment>(payment),
            ObjectId.class
        );

        this.restTemplate.delete(root + "payments/" + response.getBody());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    public void postPaymentShouldReturnAnIdThatIsRetrievable() throws Exception {
        ObjectId orgid = new ObjectId();
        Payment payment = new Payment(orgid);

        ObjectId id = this.restTemplate.exchange(
            root + "payments", 
            HttpMethod.POST, 
            new HttpEntity<Payment>(payment),
            ObjectId.class
        ).getBody();

        ResponseEntity<Payment> response = this.restTemplate.getForEntity(root + "payments/" + id, Payment.class);

        this.restTemplate.delete(root + "payments/" + id);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getOrganisationId()).isEqualTo(orgid);
    }

    @Test
    public void getSinglePaymentShouldReturnPayment() throws Exception {
        ObjectId orgId = new ObjectId();
        Payment payment = new Payment(orgId);
        ObjectId id = this.restTemplate.postForObject(root + "payments", payment, ObjectId.class);

        ResponseEntity<Payment> response = this.restTemplate.getForEntity(root + "payments/" + id, Payment.class);

        this.restTemplate.delete(root + "payments/" + id);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getOrganisationId()).isEqualTo(orgId);
    }

    @Test
    public void getSinglePaymentShouldReturnAttributesObjectWithinPaymentObject() throws Exception {
        ObjectId orgId = new ObjectId();
        Payment payment = new Payment(orgId);
        Attributes attributes = new Attributes(100.00);
        payment.setAttributes(attributes);
        ObjectId id = this.restTemplate.postForObject(root + "payments", payment, ObjectId.class);

        ResponseEntity<Payment> response = this.restTemplate.getForEntity(root + "payments/" + id, Payment.class);

        this.restTemplate.delete(root + "payments/" + id);

        Payment paymentFromApi = response.getBody();
        Attributes attributesFromApi = paymentFromApi.getAttributes();

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(attributesFromApi.getAmount()).isEqualTo(attributes.getAmount());
    }

    @Test
    public void updateAttributesObjectWithinPaymentObject() throws Exception {
        ObjectId orgId = new ObjectId();
        Payment payment = new Payment(orgId);
        Attributes attributes = new Attributes(100.00);
        payment.setAttributes(attributes);
        ObjectId id = this.restTemplate.postForObject(root + "payments", payment, ObjectId.class);
        payment.setId(id);

        Attributes updatedAttributes = new Attributes(200.00);
        payment.setAttributes(updatedAttributes);
        this.restTemplate.put(root + "payments/" + id, payment);

        ResponseEntity<Payment> response = this.restTemplate.getForEntity(root + "payments/" + id, Payment.class);

        this.restTemplate.delete(root + "payments/" + id);

        Payment paymentFromApi = response.getBody();
        Attributes attributesFromApi = paymentFromApi.getAttributes();

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(attributesFromApi.getAmount()).isEqualTo(updatedAttributes.getAmount());
    }

    @Test
    public void getSinglePaymentShouldReturnBeneficiaryPartyObjectWithinAttributesObject() throws Exception {
        ObjectId orgId = new ObjectId();
        Payment payment = new Payment(orgId);
        Attributes attributes = new Attributes(100.00);
        String accountName = "W Owens";
        BeneficiaryParty beneficiaryParty = new BeneficiaryParty(accountName);
        attributes.setBeneficiaryParty(beneficiaryParty);
        payment.setAttributes(attributes);
        ObjectId id = this.restTemplate.postForObject(root + "payments", payment, ObjectId.class);

        ResponseEntity<Payment> response = this.restTemplate.getForEntity(root + "payments/" + id, Payment.class);

        this.restTemplate.delete(root + "payments/" + id);

        BeneficiaryParty beneficiaryPartyFromApi = response.getBody().getAttributes().getBeneficiaryParty();


        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(beneficiaryPartyFromApi.getAccountName()).isEqualTo(accountName);
    }

    @Test
    public void updateBeneficiaryPartyObjectWithinAttributesObject() throws Exception {
        ObjectId orgId = new ObjectId();
        Payment payment = new Payment(orgId);
        Attributes attributes = new Attributes();
        BeneficiaryParty beneficiaryParty = new BeneficiaryParty();
        beneficiaryParty.setAccountName("old");
        attributes.setBeneficiaryParty(beneficiaryParty);
        payment.setAttributes(attributes);
        ObjectId id = this.restTemplate.postForObject(root + "payments", payment, ObjectId.class);
        payment.setId(id);

        String expected = "new";
        beneficiaryParty.setAccountName(expected);
        attributes.setBeneficiaryParty(beneficiaryParty);
        payment.setAttributes(attributes);
        this.restTemplate.put(root + "payments/" + id, payment);

        ResponseEntity<Payment> response = this.restTemplate.getForEntity(root + "payments/" + id, Payment.class);

        this.restTemplate.delete(root + "payments/" + id);

        Payment paymentFromApi = response.getBody();
        Attributes attributesFromApi = paymentFromApi.getAttributes();

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(attributesFromApi.getBeneficiaryParty().getAccountName()).isEqualTo(expected);
    }

    @Test
    public void updateChargesInformationObjectWithinAttributesObject() throws Exception {
        ObjectId orgId = new ObjectId();
        Payment payment = new Payment(orgId);
        Attributes attributes = new Attributes();
        ChargesInformation chargesInformation = new ChargesInformation();
        chargesInformation.setBearerCode("old");
        attributes.setChargesInformation(chargesInformation);
        payment.setAttributes(attributes);
        ObjectId id = this.restTemplate.postForObject(root + "payments", payment, ObjectId.class);
        payment.setId(id);

        String expected = "SHAR";
        chargesInformation.setBearerCode(expected);
        attributes.setChargesInformation(chargesInformation);
        payment.setAttributes(attributes);
        this.restTemplate.put(root + "payments/" + id, payment);

        ResponseEntity<Payment> response = this.restTemplate.getForEntity(root + "payments/" + id, Payment.class);

        this.restTemplate.delete(root + "payments/" + id);

        Payment paymentFromApi = response.getBody();
        Attributes attributesFromApi = paymentFromApi.getAttributes();

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(attributesFromApi.getChargesInformation().getBearerCode()).isEqualTo(expected);
    }

    @Test
    public void updateSenderChargesObjectWithinChargesInformationObject() throws Exception {
        ObjectId orgId = new ObjectId();
        Payment payment = new Payment(orgId);
        Attributes attributes = new Attributes();
        SenderCharges charge1 = new SenderCharges(100.0, "GBP");
        List<SenderCharges> senderCharges1 = new ArrayList<>();
        senderCharges1.add(charge1);
        ChargesInformation chargesInformation = new ChargesInformation();
        chargesInformation.setSenderCharges(senderCharges1);
        attributes.setChargesInformation(chargesInformation);
        payment.setAttributes(attributes);
        ObjectId id = this.restTemplate.postForObject(root + "payments", payment, ObjectId.class);
        payment.setId(id);


        String expected = "USD";
        SenderCharges charge2 = new SenderCharges(100.0, expected);
        List<SenderCharges> senderCharges2 = new ArrayList<>();
        senderCharges2.add(charge2);
        chargesInformation.setSenderCharges(senderCharges2);
        attributes.setChargesInformation(chargesInformation);
        payment.setAttributes(attributes);
        this.restTemplate.put(root + "payments/" + id, payment);

        ResponseEntity<Payment> response = this.restTemplate.getForEntity(root + "payments/" + id, Payment.class);

        this.restTemplate.delete(root + "payments/" + id);

        Payment paymentFromApi = response.getBody();
        Attributes attributesFromApi = paymentFromApi.getAttributes();

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(attributesFromApi.getChargesInformation().getSenderCharges().get(0).getCurrency()).isEqualTo(expected);
    }

    @Test
    public void getSinglePaymentsWithNoPaymentsShouldReturnNotFound() throws Exception {
        String id = new ObjectId().toString();
        ResponseEntity<String> response = this.restTemplate.getForEntity(root + "payments/" + id, String.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void deletePaymentShouldRemovePayment() throws Exception {
        Payment payment = new Payment(new ObjectId());
        ObjectId id = this.restTemplate.postForObject(root + "payments", payment, ObjectId.class);

        this.restTemplate.delete(root + "payments/" + id);

        ResponseEntity<String> response = this.restTemplate.getForEntity(root + "payments/" + id, String.class);

        assertThat(response.getBody()).isNullOrEmpty();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void deletePaymentsWithNoPaymentsShouldReturnNotFound() throws Exception {
        String id = new ObjectId().toString();
        ResponseEntity<String> response = this.restTemplate.exchange(
            root + "payments/" + id, 
            HttpMethod.DELETE, 
            null, 
            String.class
        );
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void putUsedToUpdatePaymentShouldChangePaymentInDatabase() throws Exception {
        ObjectId oldOrgId = new ObjectId();
        ObjectId newOrgId = new ObjectId();
        Payment payment = new Payment(oldOrgId);
        ObjectId id = this.restTemplate.postForObject(root + "payments", payment, ObjectId.class);
        payment.setId(id);
        payment.setOrganisationId(newOrgId);

        this.restTemplate.put(root + "payments/" + id, payment);
        ResponseEntity<Payment> response = this.restTemplate.getForEntity(root + "payments/" + id, Payment.class);

        this.restTemplate.delete(root + "payments/" + id);

        assertThat(response.getBody().getOrganisationId()).isEqualTo(newOrgId);
    }

    @Test
    public void putPaymentWithNoPaymentsShouldReturnNotFound() throws Exception {
        String id = new ObjectId().toString();
        ObjectId orgid = new ObjectId();
        Payment payment = new Payment(orgid);

        ResponseEntity<String> response = this.restTemplate.exchange(
            root + "payments/" + id, 
            HttpMethod.PUT, 
            new HttpEntity<Payment>(payment),
            String.class
        );

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void getAllPaymentsShouldReturnAllPayments() throws Exception {
        ObjectId orgId1 = new ObjectId();
        Payment payment1 = new Payment(orgId1);
        ObjectId orgId2 = new ObjectId();
        Payment payment2 = new Payment(orgId2);

        ObjectId id1 = this.restTemplate.postForObject(root + "payments", payment1, ObjectId.class);
        ObjectId id2 = this.restTemplate.postForObject(root + "payments", payment2, ObjectId.class);

        ResponseEntity<List<Payment>> response = this.restTemplate.exchange(
            root + "payments", 
            HttpMethod.GET, 
            null,
            new ParameterizedTypeReference<List<Payment>>(){});

        this.restTemplate.delete(root + "payments/" + id1);
        this.restTemplate.delete(root + "payments/" + id2);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().get(0).getOrganisationId()).isEqualTo(orgId1);
        assertThat(response.getBody().get(1).getOrganisationId()).isEqualTo(orgId2);
    }

    @Test
    public void getAllPaymentsWithNoPaymentsShouldReturnNotFound() throws Exception {
        ResponseEntity<List<Payment>> response = this.restTemplate.exchange(
            root + "payments", 
            HttpMethod.GET, 
            null,
            new ParameterizedTypeReference<List<Payment>>(){});

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody().size()).isEqualTo(0);
    }

}
