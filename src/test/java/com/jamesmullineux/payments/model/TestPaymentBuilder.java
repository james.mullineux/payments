package com.jamesmullineux.payments.model;

import org.bson.types.ObjectId;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.time.LocalDate;

public class TestPaymentBuilder {
    Payment payment = new Payment(new ObjectId());
    Attributes attributes = new Attributes(100.00, "1010101", "000000000000000001", "Paying for something", "Scheme", "Some type", LocalDate.now(), "Some reference", "Some kind of banking", "Some kind of payment", "CUR", "Some end to end reference");
    BeneficiaryParty beneficiaryParty = new BeneficiaryParty("D efault", "12345678", "ABCD", 1, "7 Address Place NoTown AB1 2CD", "111999", "USABC", "A Name");
    Fx fx = new Fx("Some contract reference", new Money(2.00000, 5), new Money(100.0), "some original currency");
    SponsorParty sponsorParty = new SponsorParty("Some Account number", "Some bank id", "Some bank id code");
    ChargesInformation chargesInformation = new ChargesInformation("AAAA");
    List<SenderCharges> senderCharges = Stream.of(new SenderCharges(0.00, "ABC")).collect(Collectors.toList());
    TransactingParty debtorParty = new TransactingParty("D efault", "12345678", "ABCD", "7 Address Place NoTown AB1 2CD", "111999", "USABC", "A Name");

    public TestPaymentBuilder withBeneficiaryPartyAccountNumber(String number) {
        beneficiaryParty.setAccountNumber(number);
        return this;
    }

    public TestPaymentBuilder withBeneficiaryPartyAccountNumberCode(String code) {
        beneficiaryParty.setAccountNumberCode(code);
        return this;
    }

    public TestPaymentBuilder withBeneficiaryPartyAccountType(int type) {
        beneficiaryParty.setAccountType(type);
        return this;
    }

    public TestPaymentBuilder withAttributesAmount(Money amount) {
        attributes.setAmount(amount.getValue());
        return this;
    }

    public TestPaymentBuilder withBeneficiaryPartyAddress(String address) {
        beneficiaryParty.setAddress(address);
        return this;
    }

    public TestPaymentBuilder withBeneficiaryPartyBankId(String bankId) {
        beneficiaryParty.setBankId(bankId);
        return this;
    }

    public TestPaymentBuilder withBeneficiaryPartyBankIdCode(String bankIdCode) {
        beneficiaryParty.setBankIdCode(bankIdCode);
        return this;
    }

    public TestPaymentBuilder withBeneficiaryPartyName(String name) {
        beneficiaryParty.setName(name);
        return this;
    }

    public TestPaymentBuilder withNumericReference(String numericReference) {
        attributes.setNumericReference(numericReference);
        return this;
    }

    public TestPaymentBuilder withPaymentId(String paymentId) {
        attributes.setPaymentId(paymentId);
        return this;
    }

    public TestPaymentBuilder withPaymentPurpose(String paymentPurpose) {
        attributes.setPaymentPurpose(paymentPurpose);
        return this;
    }

    public TestPaymentBuilder withPaymentScheme(String paymentScheme) {
        attributes.setPaymentScheme(paymentScheme);
        return this;
    }

    public TestPaymentBuilder withPaymentType(String paymentType) {
        attributes.setPaymentType(paymentType);
        return this;
    }

    public TestPaymentBuilder withProcessingDate(LocalDate processingDate) {
        attributes.setProcessingDate(processingDate);
        return this;
    }

    public TestPaymentBuilder withReference(String reference) {
        attributes.setReference(reference);
        return this;
    }

    public TestPaymentBuilder withSchemePaymentSubType(String schemePaymentSubType) {
        attributes.setSchemePaymentSubType(schemePaymentSubType);
        return this;
    }

    public TestPaymentBuilder withSchemePaymentType(String schemePaymentType) {
        attributes.setSchemePaymentType(schemePaymentType);
        return this;
    }

    public TestPaymentBuilder withCurrency(String currency) {
        attributes.setCurrency(currency);
        return this;
    }

    public TestPaymentBuilder withEndToEndReference(String endToEndReference) {
        attributes.setEndToEndReference(endToEndReference);
        return this;
    }

    public TestPaymentBuilder withContractReference(String contractReference) {
        fx.setContractReference(contractReference);
        return this;
    }

    public TestPaymentBuilder withExchangeRate(Money exchangeRate) {
        fx.setExchangeRate(exchangeRate);
        return this;
    }

    public TestPaymentBuilder withOriginalAmount(Money originalAmount) {
        fx.setOriginalAmount(originalAmount);
        return this;
    }

    public TestPaymentBuilder withOriginalCurrency(String originalCurrency) {
        fx.setOriginalCurrency(originalCurrency);
        return this;
    }

    public TestPaymentBuilder withSponsorPartyAccountNumber(String accountNumber) {
        sponsorParty.setAccountNumber(accountNumber);
        return this;
    }

    public TestPaymentBuilder withSponsorPartyBankId(String bankId) {
        sponsorParty.setBankId(bankId);
        return this;
    }

    public TestPaymentBuilder withSponsorPartyBankIdCode(String bankIdCode) {
        sponsorParty.setBankIdCode(bankIdCode);
        return this;
    }

    public TestPaymentBuilder withChargesInformationBearerCode(String bearerCode) {
        chargesInformation.setBearerCode(bearerCode);
        return this;
    }

    public TestPaymentBuilder withReceiverChargesAmount(Money receiverChargesAmount) {
        chargesInformation.setReceiverChargesAmount(receiverChargesAmount);
        return this;
    }

    public TestPaymentBuilder withReceiverChargesCurrency(String receiverChargesCurrency) {
        chargesInformation.setReceiverChargesCurrency(receiverChargesCurrency);
        return this;
    }

    public TestPaymentBuilder withSenderCharges(SenderCharges senderCharges) {
        this.senderCharges.add(senderCharges);
        return this;
    }

    public TestPaymentBuilder withSenderChargesAmount(double amount) {
        this.senderCharges.add(new SenderCharges(amount, ""));
        return this;
    }

    public TestPaymentBuilder withSenderChargesCurrency(String currency) {
        this.senderCharges.add(new SenderCharges(0.00, currency));
        return this;
    }

    public TestPaymentBuilder withDebtorPartyAccountName(String name) {
        debtorParty.setAccountName(name);
        return this;
    }

    public TestPaymentBuilder withDebtorPartyAccountNumber(String number) {
        debtorParty.setAccountNumber(number);
        return this;
    }

    public TestPaymentBuilder withDebtorPartyAccountNumberCode(String code) {
        debtorParty.setAccountNumberCode(code);
        return this;
    }

    public TestPaymentBuilder withDebtorPartyAddress(String address) {
        debtorParty.setAddress(address);
        return this;
    }

    public TestPaymentBuilder withDebtorPartyBankId(String bankId) {
        debtorParty.setBankId(bankId);
        return this;
    }

    public TestPaymentBuilder withDebtorPartyBankIdCode(String bankIdCode) {
        debtorParty.setBankIdCode(bankIdCode);
        return this;
    }

    public TestPaymentBuilder withDebtorPartyName(String name) {
        debtorParty.setName(name);
        return this;
    }

    public Payment build() {
        chargesInformation.setSenderCharges(senderCharges);
        attributes.setDebtorParty(debtorParty);
        attributes.setChargesInformation(chargesInformation);
        attributes.setSponsorParty(sponsorParty);
        attributes.setFx(fx);
        attributes.setBeneficiaryParty(beneficiaryParty);
        payment.setAttributes(attributes);
        return payment;
    }
}
