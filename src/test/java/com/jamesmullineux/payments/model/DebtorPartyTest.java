package com.jamesmullineux.payments.model;

import org.junit.Before;
import org.junit.Test;
import org.bson.types.ObjectId;
import static org.assertj.core.api.Assertions.assertThat;
import com.jamesmullineux.payments.model.Payment;
import com.jamesmullineux.payments.manager.PaymentManager;

public class DebtorPartyTest {

    private PaymentManager paymentManager;

    @Before
    public void beforeEach() {
        paymentManager = new PaymentManager();
    }

    @Test
    public void testCreateDebtorPartyInAttributes() {
        Payment original = new TestPaymentBuilder()
            .build();
        ObjectId id = paymentManager.save(original);

        TransactingParty fromDatabase = paymentManager.payment(id).get().getAttributes().getDebtorParty();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase).isNotNull();
    }

    @Test
    public void testCreateNameInDebtorParty() {
        String accountName = "Emelia Jane Brown";
        Payment original = new TestPaymentBuilder()
            .withDebtorPartyName(accountName)
            .build();
        ObjectId id = paymentManager.save(original);

        TransactingParty fromDatabase = paymentManager.payment(id).get().getAttributes().getDebtorParty();

        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getName()).isEqualTo(accountName);
    }

    @Test
    public void testCreateAccountNameInDebtorParty() {
        String accountName = "EJ Brown Black";
        Payment original = new TestPaymentBuilder()
            .withDebtorPartyAccountName(accountName)
            .build();
        ObjectId id = paymentManager.save(original);

        TransactingParty fromDatabase = paymentManager.payment(id).get().getAttributes().getDebtorParty();

        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getAccountName()).isEqualTo(accountName);
    }

    @Test
    public void testCreateAccountNumberCodeInDebtorParty() {
        String accountNumberCode = "IBAN";
        Payment original = new TestPaymentBuilder()
            .withDebtorPartyAccountNumberCode(accountNumberCode)
            .build();
        ObjectId id = paymentManager.save(original);

        TransactingParty fromDatabase = paymentManager.payment(id).get().getAttributes().getDebtorParty();

        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getAccountNumberCode()).isEqualTo(accountNumberCode);
    }

    @Test
    public void testCreateAccountNumberInDebtorParty() {
        String accountNumber = "GB29XABC10161234567801";
        Payment original = new TestPaymentBuilder()
            .withDebtorPartyAccountNumber(accountNumber)
            .build();
        ObjectId id = paymentManager.save(original);

        TransactingParty fromDatabase = paymentManager.payment(id).get().getAttributes().getDebtorParty();

        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getAccountNumber()).isEqualTo(accountNumber);
    }

    @Test
    public void testCreateAddressInDebtorParty() {
        String address = "10 Debtor Crescent Sourcetown NE1";
        Payment original = new TestPaymentBuilder()
            .withDebtorPartyAddress(address)
            .build();
        ObjectId id = paymentManager.save(original);

        TransactingParty fromDatabase = paymentManager.payment(id).get().getAttributes().getDebtorParty();

        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getAddress()).isEqualTo(address);
    }

    @Test
    public void testCreateBankIdInDebtorParty() {
        String bankId = "203301";
        Payment original = new TestPaymentBuilder()
            .withDebtorPartyBankId(bankId)
            .build();
        ObjectId id = paymentManager.save(original);

        TransactingParty fromDatabase = paymentManager.payment(id).get().getAttributes().getDebtorParty();

        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getBankId()).isEqualTo(bankId);
    }

    @Test
    public void testCreateBankIdCodeInDebtorParty() {
        String bankIdCode = "GBDSC";
        Payment original = new TestPaymentBuilder()
            .withDebtorPartyBankIdCode(bankIdCode)
            .build();
        ObjectId id = paymentManager.save(original);

        TransactingParty fromDatabase = paymentManager.payment(id).get().getAttributes().getDebtorParty();

        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getBankIdCode()).isEqualTo(bankIdCode);
    }

}
