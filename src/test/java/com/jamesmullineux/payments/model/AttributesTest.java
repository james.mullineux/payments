package com.jamesmullineux.payments.model;

import org.junit.Before;
import org.junit.Test;
import org.bson.types.ObjectId;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;

import com.jamesmullineux.payments.model.Payment;
import com.jamesmullineux.payments.manager.PaymentManager;

public class AttributesTest {

    private PaymentManager paymentManager;

    @Before
    public void beforeEach() {
        paymentManager = new PaymentManager();
    }

    @Test
    public void testCreateAttributesInAPayment() {
        Payment original = new TestPaymentBuilder().build();
        ObjectId id = paymentManager.save(original);
        Payment fromDatabase = paymentManager.payment(id).get();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getAttributes().getAmount()).isEqualTo(original.getAttributes().getAmount());
    }

    @Test
    public void testAttributeAmountsAreSavedAsMoney() {
        Payment original = new TestPaymentBuilder()
            .withAttributesAmount(new Money(100.219999))
            .build();
        ObjectId id = paymentManager.save(original);
        Payment fromDatabase = paymentManager.payment(id).get();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getAttributes().getAmount()).isEqualTo(100.22);
    }

    @Test
    public void testCreateNumericReferenceInAttributes() {
        Payment original = new TestPaymentBuilder()
            .withNumericReference("1002001")
            .build();

        Attributes attributes = original.getAttributes();
        ObjectId id = paymentManager.save(original);
        Attributes fromDatabase = paymentManager.payment(id).get().getAttributes();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getNumericReference()).isEqualTo(attributes.getNumericReference());
    }

    @Test
    public void testCreatePaymentIdInAttributes() {
        Payment original = new TestPaymentBuilder()
            .withPaymentId("123456789012345678")
            .build();

        Attributes attributes = original.getAttributes();
        ObjectId id = paymentManager.save(original);
        Attributes fromDatabase = paymentManager.payment(id).get().getAttributes();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getPaymentId()).isEqualTo(attributes.getPaymentId());
    }

    @Test
    public void testCreatePaymentPurposeInAttributes() {
        Payment original = new TestPaymentBuilder()
            .withPaymentPurpose("Paying for goods/services")
            .build();

        Attributes attributes = original.getAttributes();
        ObjectId id = paymentManager.save(original);
        Attributes fromDatabase = paymentManager.payment(id).get().getAttributes();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getPaymentPurpose()).isEqualTo(attributes.getPaymentPurpose());
    }

    @Test
    public void testCreatePaymentSchemeInAttributes() {
        Payment original = new TestPaymentBuilder()
            .withPaymentScheme("FPS")
            .build();

        Attributes attributes = original.getAttributes();
        ObjectId id = paymentManager.save(original);
        Attributes fromDatabase = paymentManager.payment(id).get().getAttributes();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getPaymentScheme()).isEqualTo(attributes.getPaymentScheme());
    }

    @Test
    public void testCreatePaymentTypeInAttributes() {
        Payment original = new TestPaymentBuilder()
            .withPaymentType("Credit")
            .build();

        Attributes attributes = original.getAttributes();
        ObjectId id = paymentManager.save(original);
        Attributes fromDatabase = paymentManager.payment(id).get().getAttributes();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getPaymentType()).isEqualTo(attributes.getPaymentType());
    }

    @Test
    public void testCreateProcessingDateInAttributes() {
        Payment original = new TestPaymentBuilder()
            .withProcessingDate(LocalDate.parse("2017-01-18"))
            .build();

        Attributes attributes = original.getAttributes();
        ObjectId id = paymentManager.save(original);
        Attributes fromDatabase = paymentManager.payment(id).get().getAttributes();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getProcessingDate()).isEqualTo(attributes.getProcessingDate());
    }

    @Test
    public void testCreateReferenceInAttributes() {
        Payment original = new TestPaymentBuilder()
            .withReference("Payment for Em's piano lessons")
            .build();

        Attributes attributes = original.getAttributes();
        ObjectId id = paymentManager.save(original);
        Attributes fromDatabase = paymentManager.payment(id).get().getAttributes();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getReference()).isEqualTo(attributes.getReference());
    }

    @Test
    public void testCreateSchemePaymentSubTypeInAttributes() {
        Payment original = new TestPaymentBuilder()
            .withSchemePaymentSubType("InternetBanking")
            .build();

        Attributes attributes = original.getAttributes();
        ObjectId id = paymentManager.save(original);
        Attributes fromDatabase = paymentManager.payment(id).get().getAttributes();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getSchemePaymentSubType()).isEqualTo(attributes.getSchemePaymentSubType());
    }

    @Test
    public void testCreateSchemePaymentTypeInAttributes() {
        Payment original = new TestPaymentBuilder()
            .withSchemePaymentType("ImmediatePayment")
            .build();

        Attributes attributes = original.getAttributes();
        ObjectId id = paymentManager.save(original);
        Attributes fromDatabase = paymentManager.payment(id).get().getAttributes();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getSchemePaymentType()).isEqualTo(attributes.getSchemePaymentType());
    }

    @Test
    public void testCreateCurrencyInAttributes() {
        Payment original = new TestPaymentBuilder()
            .withCurrency("GBP")
            .build();

        Attributes attributes = original.getAttributes();
        ObjectId id = paymentManager.save(original);
        Attributes fromDatabase = paymentManager.payment(id).get().getAttributes();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getCurrency()).isEqualTo(attributes.getCurrency());
    }

    @Test
    public void testEndToEndReferenceInAttributes() {
        Payment original = new TestPaymentBuilder()
            .withEndToEndReference("Wil piano Jan")
            .build();

        Attributes attributes = original.getAttributes();
        ObjectId id = paymentManager.save(original);
        Attributes fromDatabase = paymentManager.payment(id).get().getAttributes();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getEndToEndReference()).isEqualTo(attributes.getEndToEndReference());
    }

}
