package com.jamesmullineux.payments.model;

import org.junit.Before;
import org.junit.Test;
import org.bson.types.ObjectId;
import static org.assertj.core.api.Assertions.assertThat;
import com.jamesmullineux.payments.model.Payment;
import com.jamesmullineux.payments.manager.PaymentManager;

public class SponsorPartyTest {

    private PaymentManager paymentManager;

    @Before
    public void beforeEach() {
        paymentManager = new PaymentManager();
    }

    @Test
    public void testCreateSponsorPartyInAttributes() {
        Payment original = new TestPaymentBuilder()
            .build();
        ObjectId id = paymentManager.save(original);

        SponsorParty fromDatabase = paymentManager.payment(id).get().getAttributes().getSponsorParty();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase).isNotNull();
    }

    @Test
    public void testCreateAccountNumberInSponsorParty() {
        String accountNumber = "56781234";
        Payment original = new TestPaymentBuilder()
            .withSponsorPartyAccountNumber(accountNumber)
            .build();
        ObjectId id = paymentManager.save(original);

        SponsorParty fromDatabase = paymentManager.payment(id).get().getAttributes().getSponsorParty();

        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getAccountNumber()).isEqualTo(accountNumber);
    }

    @Test
    public void testCreateBankIdInSponsorParty() {
        String bankId = "123123";
        Payment original = new TestPaymentBuilder()
            .withSponsorPartyBankId(bankId)
            .build();
        ObjectId id = paymentManager.save(original);

        SponsorParty fromDatabase = paymentManager.payment(id).get().getAttributes().getSponsorParty();

        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getBankId()).isEqualTo(bankId);
    }

    @Test
    public void testCreateBankIdCodeInSponsorParty() {
        String bankIdCode = "GBDSC";
        Payment original = new TestPaymentBuilder()
            .withSponsorPartyBankIdCode(bankIdCode)
            .build();
        ObjectId id = paymentManager.save(original);

        SponsorParty fromDatabase = paymentManager.payment(id).get().getAttributes().getSponsorParty();

        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getBankIdCode()).isEqualTo(bankIdCode);
    }

}
