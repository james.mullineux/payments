package com.jamesmullineux.payments.model;

import org.junit.Before;
import org.junit.Test;
import org.bson.types.ObjectId;
import static org.assertj.core.api.Assertions.assertThat;
import com.jamesmullineux.payments.model.Payment;
import com.jamesmullineux.payments.manager.PaymentManager;

public class MoneyTest {

    private PaymentManager paymentManager;

    @Before
    public void beforeEach() {
        paymentManager = new PaymentManager();
    }

    @Test
    public void testAttributeAmountsAreSetToTwoDecimalPlaces() {
        Payment original = new TestPaymentBuilder()
            .withAttributesAmount(new Money(100.219999))
            .build();
        ObjectId id = paymentManager.save(original);
        Payment fromDatabase = paymentManager.payment(id).get();
        paymentManager.delete(id);
        paymentManager.close();

        double actual = fromDatabase.getAttributes().getAmount();
        assertThat(actual).isEqualTo(100.22);
    }

    @Test
    public void testMoneyAmountsAreRoundedUp() {
        Payment original = new TestPaymentBuilder()
            .withAttributesAmount(new Money(99.999))
            .build();
        ObjectId id = paymentManager.save(original);
        Payment fromDatabase = paymentManager.payment(id).get();
        paymentManager.delete(id);
        paymentManager.close();

        double actual = fromDatabase.getAttributes().getAmount();
        assertThat(actual).isEqualTo(100);
    }

    @Test
    public void testMoneyAmountsAreRoundedHalfUp() {
        Payment original = new TestPaymentBuilder()
            .withAttributesAmount(new Money(99.995))
            .build();
        ObjectId id = paymentManager.save(original);
        Payment fromDatabase = paymentManager.payment(id).get();
        paymentManager.delete(id);
        paymentManager.close();

        double actual = fromDatabase.getAttributes().getAmount();
        assertThat(actual).isEqualTo(100);
    }

    @Test
    public void testMoneyAmountsAreRoundedDownUp() {
        Payment original = new TestPaymentBuilder()
            .withAttributesAmount(new Money(99.994))
            .build();
        ObjectId id = paymentManager.save(original);
        Payment fromDatabase = paymentManager.payment(id).get();
        paymentManager.delete(id);
        paymentManager.close();

        double actual = fromDatabase.getAttributes().getAmount();
        assertThat(actual).isEqualTo(99.99);
    }

    @Test
    public void testMoneyAmountsRetainTwoDecimalPlacesWhenTheyAreZeros() {
        Payment original = new TestPaymentBuilder()
            .withAttributesAmount(new Money(0.00))
            .build();
        ObjectId id = paymentManager.save(original);
        Payment fromDatabase = paymentManager.payment(id).get();
        paymentManager.delete(id);
        paymentManager.close();

        Money actual = new Money(fromDatabase.getAttributes().getAmount());
        assertThat(actual.toString()).isEqualTo("0.00");
    }

}