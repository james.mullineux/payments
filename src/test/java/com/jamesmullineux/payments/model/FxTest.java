package com.jamesmullineux.payments.model;

import org.junit.Before;
import org.junit.Test;
import org.bson.types.ObjectId;
import static org.assertj.core.api.Assertions.assertThat;
import com.jamesmullineux.payments.model.Payment;
import com.jamesmullineux.payments.manager.PaymentManager;

public class FxTest {

    private PaymentManager paymentManager;

    @Before
    public void beforeEach() {
        paymentManager = new PaymentManager();
    }

    @Test
    public void testCreateSponsorPartyInAttributes() {
        Payment original = new TestPaymentBuilder()
            .build();
        ObjectId id = paymentManager.save(original);

        Fx fromDatabase = paymentManager.payment(id).get().getAttributes().getFx();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase).isNotNull();
    }

    @Test
    public void testCreateContractReferenceInFx() {
        String contractReference = "FX123";
        Payment original = new TestPaymentBuilder()
            .withContractReference(contractReference)
            .build();

        Attributes attributes = original.getAttributes();
        ObjectId id = paymentManager.save(original);
        Attributes fromDatabase = paymentManager.payment(id).get().getAttributes();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getFx().getContractReference()).isEqualTo(attributes.getFx().getContractReference());
    }

    @Test
    public void testCreateExchangeRateInFx() {
        Money exchangeRate = new Money(2.00000, 5);
        Payment original = new TestPaymentBuilder()
            .withExchangeRate(exchangeRate)
            .build();

        ObjectId id = paymentManager.save(original);
        Fx fromDatabase = paymentManager.payment(id).get().getAttributes().getFx();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getExchangeRate().getValue()).isEqualTo(exchangeRate.getValue());
    }

    @Test
    public void testCreateOriginalAmountInFx() {
        Money originalAmount = new Money(200.42);
        Payment original = new TestPaymentBuilder()
            .withOriginalAmount(originalAmount)
            .build();

        ObjectId id = paymentManager.save(original);
        Fx fromDatabase = paymentManager.payment(id).get().getAttributes().getFx();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getOriginalAmount().getValue()).isEqualTo(originalAmount.getValue());
    }

    @Test
    public void testCreateOriginalCurrencyInFx() {
        String originalCurrency = "USD";
        Payment original = new TestPaymentBuilder()
            .withOriginalCurrency(originalCurrency)
            .build();

        ObjectId id = paymentManager.save(original);
        Fx fromDatabase = paymentManager.payment(id).get().getAttributes().getFx();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getOriginalCurrency()).isEqualTo(originalCurrency);
    }

}
