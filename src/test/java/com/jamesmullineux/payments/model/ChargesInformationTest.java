package com.jamesmullineux.payments.model;

import org.junit.Before;
import org.junit.Test;
import java.util.List;
import org.bson.types.ObjectId;
import static org.assertj.core.api.Assertions.assertThat;
import com.jamesmullineux.payments.model.Payment;
import com.jamesmullineux.payments.manager.PaymentManager;

public class ChargesInformationTest {

    private PaymentManager paymentManager;

    @Before
    public void beforeEach() {
        paymentManager = new PaymentManager();
    }

    @Test
    public void testCreateChargesInformationInAttributes() {
        Payment original = new TestPaymentBuilder()
            .build();
        ObjectId id = paymentManager.save(original);

        ChargesInformation fromDatabase = paymentManager.payment(id).get().getAttributes().getChargesInformation();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase).isNotNull();
    }

    @Test
    public void testCreateBearerCodeInChargesInformation() {
        String bearerCode = "SHAR";
        Payment original = new TestPaymentBuilder()
            .withChargesInformationBearerCode(bearerCode)
            .build();
        ObjectId id = paymentManager.save(original);

        ChargesInformation fromDatabase = paymentManager.payment(id).get().getAttributes().getChargesInformation();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getBearerCode()).isEqualTo(bearerCode);
    }

    @Test
    public void testCreateReceiverChargesAmountInChargesInformation() {
        Money receiverChargesAmount = new Money(1.00);
        Payment original = new TestPaymentBuilder()
            .withReceiverChargesAmount(receiverChargesAmount)
            .build();
        ObjectId id = paymentManager.save(original);

        ChargesInformation fromDatabase = paymentManager.payment(id).get().getAttributes().getChargesInformation();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getReceiverChargesAmount().getValue()).isEqualTo(receiverChargesAmount.getValue());
    }

    @Test
    public void testCreateReceiverChargesCurrencyInChargesInformation() {
        String receiverChargesCurrency = "USD";
        Payment original = new TestPaymentBuilder()
            .withReceiverChargesCurrency(receiverChargesCurrency)
            .build();
        ObjectId id = paymentManager.save(original);

        ChargesInformation fromDatabase = paymentManager.payment(id).get().getAttributes().getChargesInformation();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getReceiverChargesCurrency()).isEqualTo(receiverChargesCurrency);
    }

    @Test
    public void testCreateSingleSenderChargesInChargesInformation() {
        SenderCharges senderCharges = new SenderCharges(5.00, "GBP");
        Payment original = new TestPaymentBuilder()
            .withSenderCharges(senderCharges)
            .build();
        ObjectId id = paymentManager.save(original);

        List<SenderCharges> fromDatabase = paymentManager.payment(id).get().getAttributes().getChargesInformation().getSenderCharges();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.get(1).getAmount()).isEqualTo(senderCharges.getAmount());
        assertThat(fromDatabase.get(1).getCurrency()).isEqualTo(senderCharges.getCurrency());
    }


}
