package com.jamesmullineux.payments.model;

import org.junit.Before;
import org.junit.Test;
import org.bson.types.ObjectId;
import static org.assertj.core.api.Assertions.assertThat;
import com.jamesmullineux.payments.model.Payment;
import com.jamesmullineux.payments.manager.PaymentManager;

public class BeneficiaryPartyTest {

    private PaymentManager paymentManager;

    @Before
    public void beforeEach() {
        paymentManager = new PaymentManager();
    }

    @Test
    public void testCreateBeneficiaryPartyInAttributes() {
        Payment original = new TestPaymentBuilder()
            .build();
        ObjectId id = paymentManager.save(original);

        BeneficiaryParty fromDatabase = paymentManager.payment(id).get().getAttributes().getBeneficiaryParty();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase).isNotNull();
    }

    @Test
    public void testCreateAccountNumberInBeneficiaryParty() {
        String accountNumber = "31926819";
        Payment original = new TestPaymentBuilder()
            .withBeneficiaryPartyAccountNumber(accountNumber)
            .build();
        ObjectId id = paymentManager.save(original);

        BeneficiaryParty fromDatabase = paymentManager.payment(id).get().getAttributes().getBeneficiaryParty();

        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getAccountNumber()).isEqualTo(accountNumber);
    }

    @Test
    public void testCreateAccountNumberCodeInBeneficiaryParty() {
        String accountNumberCode = "BBAN";
        Payment original = new TestPaymentBuilder()
            .withBeneficiaryPartyAccountNumberCode(accountNumberCode)
            .build();
        ObjectId id = paymentManager.save(original);

        BeneficiaryParty fromDatabase = paymentManager.payment(id).get().getAttributes().getBeneficiaryParty();

        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getAccountNumberCode()).isEqualTo(accountNumberCode);
    }

    @Test
    public void testCreateAccountTypeInBeneficiaryParty() {
        int accountType = 0;
        Payment original = new TestPaymentBuilder()
            .withBeneficiaryPartyAccountType(accountType)
            .build();
        ObjectId id = paymentManager.save(original);

        BeneficiaryParty fromDatabase = paymentManager.payment(id).get().getAttributes().getBeneficiaryParty();

        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getAccountType()).isEqualTo(accountType);
    }

    @Test
    public void testCreateAddressInBeneficiaryParty() {
        String address = "1 The Beneficiary Localtown SE2";
        Payment original = new TestPaymentBuilder()
            .withBeneficiaryPartyAddress(address)
            .build();
        ObjectId id = paymentManager.save(original);

        BeneficiaryParty fromDatabase = paymentManager.payment(id).get().getAttributes().getBeneficiaryParty();

        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getAddress()).isEqualTo(address);
    }

    @Test
    public void testCreateBankIdInBeneficiaryParty() {
        String bankId = "403000";
        Payment original = new TestPaymentBuilder()
            .withBeneficiaryPartyBankId(bankId)
            .build();
        ObjectId id = paymentManager.save(original);

        BeneficiaryParty fromDatabase = paymentManager.payment(id).get().getAttributes().getBeneficiaryParty();

        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getBankId()).isEqualTo(bankId);
    }

    @Test
    public void testCreateBankIdCodeInBeneficiaryParty() {
        String bankIdCode = "GBDSC";
        Payment original = new TestPaymentBuilder()
            .withBeneficiaryPartyBankIdCode(bankIdCode)
            .build();
        ObjectId id = paymentManager.save(original);

        BeneficiaryParty fromDatabase = paymentManager.payment(id).get().getAttributes().getBeneficiaryParty();

        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getBankIdCode()).isEqualTo(bankIdCode);
    }

    @Test
    public void testCreateNameInBeneficiaryParty() {
        String name = "Wilfred Jeremiah Owens";
        Payment original = new TestPaymentBuilder()
            .withBeneficiaryPartyName(name)
            .build();
        ObjectId id = paymentManager.save(original);

        BeneficiaryParty fromDatabase = paymentManager.payment(id).get().getAttributes().getBeneficiaryParty();

        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.getName()).isEqualTo(name);
    }
}
