package com.jamesmullineux.payments.model;

import org.junit.Before;
import org.junit.Test;
import java.util.List;
import org.bson.types.ObjectId;
import static org.assertj.core.api.Assertions.assertThat;
import com.jamesmullineux.payments.model.Payment;
import com.jamesmullineux.payments.manager.PaymentManager;

public class SenderChargesTest {

    private PaymentManager paymentManager;

    @Before
    public void beforeEach() {
        paymentManager = new PaymentManager();
    }

    @Test
    public void testCreateSenderChargesInChargesInformation() {
        Payment original = new TestPaymentBuilder()
            .build();
        ObjectId id = paymentManager.save(original);

        List<SenderCharges> fromDatabase = paymentManager.payment(id).get().getAttributes().getChargesInformation().getSenderCharges();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase).isNotNull();
        assertThat(fromDatabase.size()).isEqualTo(1);
    }

    @Test
    public void testCreateSenderChargesWithAmountInChargesInformation() {
        double originalAmount = 200.42;
        Payment original = new TestPaymentBuilder()
            .withSenderChargesAmount(originalAmount)
            .build();

        ObjectId id = paymentManager.save(original);
        List<SenderCharges> fromDatabase = paymentManager.payment(id).get().getAttributes().getChargesInformation().getSenderCharges();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.get(1).getAmount()).isEqualTo(originalAmount);
    }

    @Test
    public void testCreateSenderChargesWithCurrencyInChargesInformation() {
        String originalCurrency = "USD";
        Payment original = new TestPaymentBuilder()
            .withSenderChargesCurrency(originalCurrency)
            .build();

        ObjectId id = paymentManager.save(original);
        List<SenderCharges> fromDatabase = paymentManager.payment(id).get().getAttributes().getChargesInformation().getSenderCharges();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.get(1).getCurrency()).isEqualTo(originalCurrency);
    }

    @Test
    public void testCreateMultipleSenderCharges() {
        double amount1 = 1.00;
        double amount2 = 2.00;
        Payment original = new TestPaymentBuilder()
            .withSenderCharges(new SenderCharges(amount1, "XYZ"))
            .withSenderCharges(new SenderCharges(amount2, "BFG"))
            .build();

        ObjectId id = paymentManager.save(original);
        List<SenderCharges> fromDatabase = paymentManager.payment(id).get().getAttributes().getChargesInformation().getSenderCharges();
        paymentManager.delete(id);
        paymentManager.close();

        assertThat(fromDatabase.get(1).getAmount()).isEqualTo(amount1);
        assertThat(fromDatabase.get(2).getAmount()).isEqualTo(amount2);
    }

}
