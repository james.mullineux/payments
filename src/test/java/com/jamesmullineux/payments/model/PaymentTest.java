package com.jamesmullineux.payments.model;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.bson.types.ObjectId;
import java.util.Optional;
import com.jamesmullineux.payments.model.Payment;
import com.jamesmullineux.payments.manager.PaymentManager;

public class PaymentTest {

    private PaymentManager paymentManager;

    @Before
    public void beforeEach() {
        paymentManager = new PaymentManager();
    }

    @Test
    public void testCreateAndReadPaymentFromDatabase() {
        Payment original = new TestPaymentBuilder().build();
        ObjectId id = paymentManager.save(original);
        Payment fromDatabase = paymentManager.payment(id).get();
        deleteAll(paymentManager, id);

        assertEquals(original.getOrganisationId(), fromDatabase.getOrganisationId());
    }

    @Test
    public void testCreateAndDeletePaymentFromDatabase() {
        Payment original = new TestPaymentBuilder().build();
        ObjectId id = paymentManager.save(original);
        paymentManager.delete(id);
        Optional<Payment> fromDatabase = paymentManager.payment(id);
        deleteAll(paymentManager);

        assertEquals(Optional.empty(), fromDatabase);
    }

    @Test
    public void testCreateAndUpdatePaymentFromDatabase() {
        ObjectId newOrgId = new ObjectId();

        Payment payment = new TestPaymentBuilder().build();
        ObjectId id = paymentManager.save(payment);
        payment.setOrganisationId(newOrgId);
        paymentManager.update(payment);

        Payment fromDatabase = paymentManager.payment(id).get();
        deleteAll(paymentManager, id);

        assertEquals(newOrgId, fromDatabase.getOrganisationId());
    }

    private void deleteAll(PaymentManager manager, ObjectId... ids) {
        for(ObjectId id : ids) {
            manager.delete(id);
        }
        manager.close();
    }

}
