package com.jamesmullineux.payments.model;

import org.mongodb.morphia.annotations.Embedded;

@Embedded("sender_charges")
public class SenderCharges {

    private double amount;
    private String currency;

    public SenderCharges() {}

    public SenderCharges(double amount, String currency) {
        this.amount = new Money(amount).getValue();
        this.currency = currency;
    }

    public double getAmount() {
        return new Money(this.amount).getValue();
    }

    public void setAmount(double amount) {
        this.amount = new Money(amount).getValue();
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
