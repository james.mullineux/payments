package com.jamesmullineux.payments.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Money {

    private double units;
    private transient int decimalPlaces;

    public Money(){}

    public Money(double amount) {
        this.decimalPlaces = 2;
        this.units = centsFromDollars(amount, this.decimalPlaces);
    }

    public Money(double amount, int decimalPlaces) {
        this.decimalPlaces = decimalPlaces;
        this.units = centsFromDollars(amount, decimalPlaces);
    }

    private double centsFromDollars(double amount, int decimalPlaces) {
        BigDecimal decimal = new BigDecimal(amount);
        decimal = decimal.setScale(decimalPlaces, RoundingMode.HALF_UP);
        decimal = decimal.multiply(new BigDecimal(100));
        return decimal.doubleValue();
    }

    public double getValue() {
        BigDecimal decimal = new BigDecimal(this.units);
        decimal = decimal.divide(new BigDecimal(100));
        return decimal.doubleValue();
    }

    public String toString() {
        double value = getValue();
        String output = Double.toString(value);
        if(output.indexOf(".") == output.length() - this.decimalPlaces){
            for(int i = 1; i < this.decimalPlaces; i++) {
                output += "0";
            }
        }
        return output;
    }

}