package com.jamesmullineux.payments.model;

import java.time.LocalDate;
import org.mongodb.morphia.annotations.Embedded;

@Embedded("attributes")
public class Attributes {

    private double amount;
    @Embedded
    private BeneficiaryParty beneficiary_party;
    private String numeric_reference;
    private String payment_id;
    private String payment_purpose;
    private String payment_scheme;
    private String payment_type;
    private LocalDate processing_date;
    private String reference;
    private String scheme_payment_sub_type;
    private String scheme_payment_type;
    private String currency;
    private String end_to_end_reference;
    @Embedded
    private Fx fx;
    @Embedded
    private SponsorParty sponsor_party;
    @Embedded
    private ChargesInformation charges_information;
    @Embedded
    private TransactingParty debtor_party;

    public Attributes() {}

    public Attributes(double amount) {
        this.amount = new Money(amount).getValue();
    }

    public Attributes(double amount, String numericReference, String paymentId, String paymentPurpose, String paymentScheme, String paymentType, LocalDate processingDate, String reference, String schemePaymentSubType, String schemePaymentType, String currency, String endToEndReference) {
        this.amount = new Money(amount).getValue();
        this.numeric_reference = numericReference;
        this.payment_id = paymentId;
        this.payment_purpose = paymentPurpose;
        this.payment_scheme = paymentScheme;
        this.payment_type = paymentType;
        this.processing_date = processingDate;
        this.reference = reference;
        this.scheme_payment_sub_type = schemePaymentSubType;
        this.scheme_payment_type = schemePaymentType;
        this.currency = currency;
        this.end_to_end_reference = endToEndReference;
    }

    public void setAmount(double amount) {
        this.amount = new Money(amount).getValue();
    }

    public double getAmount() {
        return this.amount;
    }

    public void setBeneficiaryParty(BeneficiaryParty beneficiaryParty) {
        this.beneficiary_party = beneficiaryParty;
    }

    public BeneficiaryParty getBeneficiaryParty() {
        return this.beneficiary_party;
    }

    public void setNumericReference(String numericReference) {
        this.numeric_reference = numericReference;
    }

    public String getNumericReference() {
        return this.numeric_reference;
    }

    public void setPaymentId(String paymentId) {
        this.payment_id = paymentId;
    }

    public String getPaymentId() {
        return this.payment_id;
    }

    public void setPaymentPurpose(String paymentPurpose) {
        this.payment_purpose = paymentPurpose;
    }

    public String getPaymentPurpose() {
        return this.payment_purpose;
    }

    public void setPaymentScheme(String paymentScheme) {
        this.payment_scheme = paymentScheme;
    }

    public String getPaymentScheme() {
        return this.payment_scheme;
    }

    public void setPaymentType(String paymentType) {
        this.payment_type = paymentType;
    }

    public String getPaymentType() {
        return this.payment_type;
    }

    public void setProcessingDate(LocalDate processingDate) {
        this.processing_date = processingDate;
    }

    public LocalDate getProcessingDate() {
        return this.processing_date;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getReference() {
        return this.reference;
    }

    public void setSchemePaymentSubType(String schemePaymentSubType) {
        this.scheme_payment_sub_type = schemePaymentSubType;
    }

    public String getSchemePaymentSubType() {
        return this.scheme_payment_sub_type;
    }

    public void setSchemePaymentType(String schemePaymentType) {
        this.scheme_payment_type = schemePaymentType;
    }

    public String getSchemePaymentType() {
        return this.scheme_payment_type;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setEndToEndReference(String endToEndReference) {
        this.end_to_end_reference = endToEndReference;
    }

    public String getEndToEndReference() {
        return this.end_to_end_reference;
    }

    public void setFx(Fx fx) {
        this.fx = fx;
    }

    public Fx getFx() {
        return this.fx;
    }

    public void setSponsorParty(SponsorParty sponsorParty) {
        this.sponsor_party = sponsorParty;
    }

    public SponsorParty getSponsorParty() {
        return this.sponsor_party;
    }

    public void setChargesInformation(ChargesInformation chargesInformation) {
        this.charges_information = chargesInformation;
    }

    public ChargesInformation getChargesInformation() {
        return this.charges_information;
    }

    public void setDebtorParty(TransactingParty debtorParty) {
        this.debtor_party = debtorParty;
    }

    public TransactingParty getDebtorParty() {
        return this.debtor_party;
    }

}
