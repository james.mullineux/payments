package com.jamesmullineux.payments.model;

import org.mongodb.morphia.annotations.Embedded;

@Embedded
public class TransactingParty extends Party {

    private String account_name;
    private String account_number_code;
    private String address;
    private String name;

    public TransactingParty() {}

    public TransactingParty(String accountName) {
        this.account_name = accountName;
    }

    public TransactingParty(String accountName, String accountNumber, String accountNumberCode, String address, String bankId, String bankIdCode, String name) {
        super(accountNumber, bankId, bankIdCode);
        this.account_name = accountName;
        this.account_number_code = accountNumberCode;
        this.address = address;
        this.name = name;
    }

    public String getAccountName() {
        return this.account_name;
    }

    public void setAccountName(String accountName) {
        this.account_name = accountName;
    }

    public String getAccountNumberCode() {
        return this.account_number_code;
    }

    public void setAccountNumberCode(String accountNumberCode) {
        this.account_number_code = accountNumberCode;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
