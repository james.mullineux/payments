package com.jamesmullineux.payments.model;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity("Payments")
public class Payment {

    @Id
    private ObjectId id;
    private String type;
    private int version;
    private ObjectId orgnaisation_id;
    @Embedded
    private Attributes attributes;

    public Payment() {}

    public Payment(ObjectId orgnaisation_id) {
        this.type = "Payment";
        this.version = 0;   
        this.orgnaisation_id = orgnaisation_id;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getVersion() {
        return this.version;
    }

    public void setOrganisationId(ObjectId orgnaisationId) {
        this.orgnaisation_id = orgnaisationId;
    }

    public ObjectId getOrganisationId() {
        return this.orgnaisation_id;
    }

    public String toString() {
        return "{ " + 
            "\"type\": \"" + this.type + "\", " + 
            "\"id\": \"" + this.id + "\", "+
            "\"version\": " + this.version + ", " +
            "\"orgnaisation_id\": \"" + this.orgnaisation_id + "\""+
        "}";
    }

}
