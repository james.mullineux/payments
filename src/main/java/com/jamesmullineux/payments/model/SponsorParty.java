package com.jamesmullineux.payments.model;

import org.mongodb.morphia.annotations.Embedded;

@Embedded("sponsor_party")
public class SponsorParty extends Party {

    public SponsorParty() {}

    public SponsorParty(String accountNumber, String bankId, String bankIdCode) {
        super(accountNumber, bankId, bankIdCode);
    }
}
