package com.jamesmullineux.payments.model;

import org.mongodb.morphia.annotations.Embedded;

@Embedded("beneficiary_party")
public class BeneficiaryParty extends TransactingParty {

    private int account_type;

    public BeneficiaryParty() {}

    public BeneficiaryParty(String accountName) {
        super(accountName);
    }

    public BeneficiaryParty(String accountName, String accountNumber, String accountNumberCode, int accountType, String address, String bankId, String bankIdCode, String name) {
        super(accountName, accountNumber, accountNumberCode, address, bankId, bankIdCode, name);
        this.account_type = accountType;
    }

    public int getAccountType() {
        return this.account_type;
    }

    public void setAccountType(int accountType) {
        this.account_type = accountType;
    }

}
