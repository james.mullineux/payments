package com.jamesmullineux.payments.model;

import org.mongodb.morphia.annotations.Embedded;
import java.util.List;
import java.util.ArrayList;

@Embedded("charges_information")
public class ChargesInformation {

    private String bearer_code;
    private Money receiver_charges_amount;
    private String receiver_charges_currency;
    @Embedded
    private List<SenderCharges> sender_charges;

    public ChargesInformation() {
        this.sender_charges = new ArrayList<>();
    }

    public ChargesInformation(String bearerCode) {
        this.bearer_code = bearerCode;
        this.sender_charges = new ArrayList<>();
    }

    public String getBearerCode() {
        return this.bearer_code;
    }

    public void setBearerCode(String bearerCode) {
        this.bearer_code = bearerCode;
    }

    public Money getReceiverChargesAmount() {
        return this.receiver_charges_amount;
    }

    public void setReceiverChargesAmount(Money receiverChargesAmount) {
        this.receiver_charges_amount = receiverChargesAmount;
    }

    public String getReceiverChargesCurrency() {
        return this.receiver_charges_currency;
    }

    public void setReceiverChargesCurrency(String receiverChargesCurrency) {
        this.receiver_charges_currency = receiverChargesCurrency;
    }

    public List<SenderCharges> getSenderCharges() {
        return this.sender_charges;
    }

    public void setSenderCharges(List<SenderCharges> senderCharges) {
        this.sender_charges = senderCharges;
    }

}
