package com.jamesmullineux.payments.model;

import org.mongodb.morphia.annotations.Embedded;

@Embedded()
public class Party {

    private String account_number;
    private String bank_id;
    private String bank_id_code;

    public Party() {}

    public Party(String accountNumber, String bankId, String bankIdCode) {
        this.account_number = accountNumber;
        this.bank_id = bankId;
        this.bank_id_code = bankIdCode;
    }

    public String getAccountNumber() {
        return this.account_number;
    }

    public void setAccountNumber(String accountNumber) {
        this.account_number = accountNumber;
    }

    public String getBankId() {
        return this.bank_id;
    }

    public void setBankId(String bankId) {
        this.bank_id = bankId;
    }

    public String getBankIdCode() {
        return this.bank_id_code;
    }

    public void setBankIdCode(String bankIdCode) {
        this.bank_id_code = bankIdCode;
    }

}
