package com.jamesmullineux.payments.model;

import org.mongodb.morphia.annotations.Embedded;

@Embedded("fx")
public class Fx {

    private String contract_reference;
    private Money exchange_rate;
    private Money original_amount;
    private String original_currency;

    public Fx() {}

    public Fx(String contractReference, Money exchangeRate, Money originalAmount, String originalCurrency) {
        this.contract_reference = contractReference;
        this.exchange_rate = exchangeRate;
        this.original_amount = originalAmount;
        this.original_currency = originalCurrency;
    }

    public String getContractReference() {
        return this.contract_reference;
    }

    public void setContractReference(String contractReference) {
        this.contract_reference = contractReference;
    }

    public Money getExchangeRate() {
        return this.exchange_rate;
    }

    public void setExchangeRate(Money exchangeRate) {
        this.exchange_rate = exchangeRate;
    }

    public Money getOriginalAmount() {
        return this.original_amount;
    }

    public void setOriginalAmount(Money originalAmount) {
        this.original_amount = originalAmount;
    }

    public String getOriginalCurrency() {
        return this.original_currency;
    }

    public void setOriginalCurrency(String originalCurrency) {
        this.original_currency = originalCurrency;
    }

}
