package com.jamesmullineux.payments.database;

import java.util.List;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.Key;
import com.mongodb.MongoClient;
import org.mongodb.morphia.Datastore;

public class MongoDatabase<T> implements CRUDDatabase<T> {

    private final Class<T> typeParameterClass;
    private final MongoClient mongoClient = new MongoClient();
    private Morphia morphia = new Morphia();
    private final Datastore datastore;
    
    public MongoDatabase(Class<T> typeParameterClass) {
        this(typeParameterClass, "test");
    }

    public MongoDatabase(Class<T> typeParameterClass, String databaseName) {
        this.typeParameterClass = typeParameterClass;
        morphia = morphia.map(typeParameterClass);
        datastore = morphia.createDatastore(mongoClient, databaseName);
    }

    public ObjectId create(T object) {
        Key<Object> key = datastore.save(object);
        return new ObjectId(key.getId().toString());
    }

    public T read(Object id) {
        return datastore.get(this.typeParameterClass, id);
    }

    public List<T> getAll() {
        return datastore.createQuery(this.typeParameterClass).asList();
    }

    public void update(Object object) {
        datastore.save(object);
    }

    public void delete(Object id) {
        datastore.delete(this.typeParameterClass, id);
    }

    public void close() {
        mongoClient.close();
    }
}