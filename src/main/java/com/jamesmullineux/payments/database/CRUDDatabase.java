package com.jamesmullineux.payments.database;

public interface CRUDDatabase<T> {
    
    Object create(T objectToCreate);
    T read(Object id);
    void update(T objectToUpdateWith);
    void delete(Object id);

}