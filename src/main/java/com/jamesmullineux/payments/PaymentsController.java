package com.jamesmullineux.payments;

import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;
import com.jamesmullineux.payments.model.Payment;
import com.jamesmullineux.payments.manager.PaymentManager;

@RestController
@RequestMapping(value = "/api/v1")
public class PaymentsController {
    
    @RequestMapping(value = "/status", 
        method = RequestMethod.GET, 
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String status() {
        return "{\"status\": \"Payments Service Up\"}";
    }

    @RequestMapping(value = "/payments", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public ResponseEntity<ObjectId> createPayment(@RequestBody Payment payment) { 
        PaymentManager paymentManager = new PaymentManager("acceptanceTests");
        ObjectId savedId = paymentManager.save(payment);
        Payment fromDatabase = paymentManager.payment(savedId).get();
        paymentManager.close();
        return new ResponseEntity<ObjectId>(fromDatabase.getId(), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/payments/{id}", method = RequestMethod.GET)
    public ResponseEntity<Payment> getPayment(@PathVariable("id") ObjectId id) {
        PaymentManager paymentManager = new PaymentManager("acceptanceTests");
        Optional<Payment> fromDatabase = paymentManager.payment(id);
        paymentManager.close();
        if(fromDatabase.isPresent()) {
            return new ResponseEntity<Payment>(fromDatabase.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<Payment>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/payments", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Payment>> getAllPayment() { 
        PaymentManager paymentManager = new PaymentManager("acceptanceTests");
        List<Payment> payments = paymentManager.allPayments();
        paymentManager.close();
        if(payments.size() > 0) {
            return new ResponseEntity<List<Payment>>(payments, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<Payment>>(new ArrayList<Payment>(), HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/payments/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Payment> updatePayment(@PathVariable("id") ObjectId id, @RequestBody Payment payment) { 
        PaymentManager paymentManager = new PaymentManager("acceptanceTests");
        Optional<Payment> fromDatabase = paymentManager.payment(id);
        if(fromDatabase.isPresent()) {
            paymentManager.update(payment);
            paymentManager.close();
            return new ResponseEntity<Payment>(HttpStatus.OK);
       } else {
            return new ResponseEntity<Payment>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/payments/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Payment> deletePayment(@PathVariable("id") ObjectId id) { 
        PaymentManager paymentManager = new PaymentManager("acceptanceTests");
        if(paymentManager.payment(id).isPresent()) {
            paymentManager.delete(id);
            paymentManager.close();
            return new ResponseEntity<Payment>(HttpStatus.OK);
        } else {
            return new ResponseEntity<Payment>(HttpStatus.NOT_FOUND);
        }
    }

}
