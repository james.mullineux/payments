package com.jamesmullineux.payments.manager;

import java.util.Optional;
import java.util.List;
import com.jamesmullineux.payments.database.MongoDatabase;
import com.jamesmullineux.payments.model.Payment;

import org.bson.types.ObjectId;

public class PaymentManager {

    private final MongoDatabase<Payment> paymentDatabase;

    public PaymentManager() {
        paymentDatabase = new MongoDatabase<Payment>(Payment.class);
    }

    public PaymentManager(String dbName) {
        paymentDatabase = new MongoDatabase<Payment>(Payment.class, dbName);
    }

    public Optional<Payment> payment(ObjectId id) {
        return Optional.ofNullable(paymentDatabase.read(id));
    }

    public ObjectId save(Payment payment) {
        return paymentDatabase.create(payment);
    }

    public void delete(ObjectId id) {
        paymentDatabase.delete(id);
    }

    public void update(Payment payment) {
        paymentDatabase.update(payment);
    }

    public void close() {
        paymentDatabase.close();
    }

    public List<Payment> allPayments() {
        return paymentDatabase.getAll();
    }

}